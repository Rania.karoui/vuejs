import { createApp } from 'vue'
import router from './router'
import App from './App.vue'


// Import our CSS
require('@/scss/main.scss');

// We create the app
createApp(App)
  .use(router)
  .mount('#app')


